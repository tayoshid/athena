/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

//  has_impl implementation
namespace ActsTrk {
namespace details {
constexpr std::optional<bool> has_impl(
    const xAOD::TrackStateContainer& trackStates, Acts::HashedString key,
    ActsTrk::IndexType istate) {
  using namespace Acts::HashedStringLiteral;
  using Acts::MultiTrajectoryTraits::kInvalid;
  INSPECTCALL(key << " " << istate);

  switch (key) {
    case "previous"_hash:
      return trackStates[istate]->previous() < kInvalid;
    case "chi2"_hash:{
      INSPECTCALL(key << " " << istate << " chi2");
      return trackStates[istate]->chi2() < kInvalid;
    }
    case "pathLength"_hash:{
      INSPECTCALL(key << " " << istate << " pathLength");
      return trackStates[istate]->pathLength() < kInvalid;
    }
    case "typeFlags"_hash:{
      INSPECTCALL(key << " " << istate << " type flags");
      return static_cast<IndexType>(trackStates[istate]->typeFlags()) < kInvalid;
    }
    case "predicted"_hash:{
      INSPECTCALL(key << " " << istate << " predicted");
      return trackStates[istate]->predicted() < kInvalid;
    }
    case "filtered"_hash:{
      INSPECTCALL(key << " " << istate << " filtered");
      return trackStates[istate]->filtered() < kInvalid;
    }
    case "smoothed"_hash:{
      INSPECTCALL(key << " " << istate << " smoothed");
      return trackStates[istate]->smoothed() < kInvalid;
    }
    case "jacobian"_hash:{
      INSPECTCALL(key << " " << istate << " jacobian");
      return trackStates[istate]->jacobian() < kInvalid;
    }
    case "projector"_hash:{
      INSPECTCALL(key << " " << istate << " projector");
      return trackStates[istate]->calibrated() < kInvalid;
    }
    case "calibrated"_hash:{
      INSPECTCALL(key << " " << istate << " calibrated");
      return trackStates[istate]->calibrated() < kInvalid;
    }
    case "calibratedCov"_hash: {
      INSPECTCALL(key << " " << istate << " calibratedCov");
      return trackStates[istate]->calibrated() < kInvalid;
    }
    case "measdim"_hash: {
      INSPECTCALL(key << " " << istate << " measdim");
      return trackStates[istate]->measDim() < kInvalid;
    }
    case "referenceSurface"_hash: {
      INSPECTCALL(key << " " << istate << " referenceSurfaceEnco");
      return true;
    }

      // TODO restore once only the EL Source Links are in use 
      // return !trackStates[istate]->uncalibratedMeasurementLink().isDefault();
  }
  INSPECTCALL(key << " " << istate << " not a predefined component");
  return std::optional<bool>();
}
}  // namespace details
}  // namespace ActsTrk


constexpr bool ActsTrk::MutableMultiTrajectory::hasColumn_impl(
    Acts::HashedString key) const {
  using namespace Acts::HashedStringLiteral;
  INSPECTCALL(key);

  switch (key) {
    case "previous"_hash:
    case "chi2"_hash:
    case "pathLength"_hash:
    case "typeFlags"_hash:
    case "predicted"_hash:
    case "filtered"_hash:
    case "smoothed"_hash:
    case "jacobian"_hash:
    case "projector"_hash:
    case "uncalibratedSourceLink"_hash:
    case "calibrated"_hash:
    case "calibratedCov"_hash:
    case "measdim"_hash:
    case "referenceSurface"_hash:
      return true;
    default:
      for (auto& d : m_decorations) {
        if (d.hash == key) {
          return true;
        }
      }
      return false;
  }
}

template <typename T>
void ActsTrk::MutableMultiTrajectory::addColumn_impl(const std::string& name) {
  // It is actually not clear if we would allow decorating RO MTJ, maybe we do
  if constexpr (ActsTrk::detail::accepted_decoration_types<T>::value) {
    m_decorations.emplace_back(
        name,
        ActsTrk::detail::decorationGetter<xAOD::TrackStateContainer, T>,
        ActsTrk::detail::decorationCopier<xAOD::TrackStateContainer, T>,
        ActsTrk::detail::decorationSetter<xAOD::TrackStateContainer, T>
  );
    // it would be useful to force presence of decoration already here
  } else {
    throw std::runtime_error("Can't add decoration of this type to MutableMultiTrajectory");
  }
}

